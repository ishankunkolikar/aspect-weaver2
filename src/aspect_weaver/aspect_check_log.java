package aspect_weaver;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import java.lang.annotation.Annotation;
import org.aspectj.lang.SoftException;

@Aspect
public class aspect_check_log {
 
    @Around("execution(* org.apache.logging.log4j.Logger.info(..))")
    public Object invoke(ProceedingJoinPoint pjp) throws Throwable {
         
    	Object[] args = pjp.getArgs();
    	
    	MethodSignature signature = (MethodSignature) pjp.getSignature();
        String methodName = signature.getMethod().getName();
        Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
        Annotation[][] annotations;
        try {
            annotations = pjp.getTarget().getClass().
                    getMethod(methodName, parameterTypes).getParameterAnnotations();
        } catch (Exception e) {
            throw new SoftException(e);
        }
        
        for (int i = 0; i < args.length; i++) {
            for (Annotation annotation : annotations[i]) {
                if (annotation.annotationType() == ReplaceFooBar.class) {
                    Object raw = args[i];
                    
                }
            }
        }

    }
}